const {createApp} = Vue; 
createApp ({
    data(){
        return {
            randomIndex: 0,
            randomIndexInternet: 0, 
            
            //vetor de imagens locais
            imagensLocais: [
                './Imagens/lua.jpg',
                './Imagens/SENAI_logo.png',
                './Imagens/sol.jpg'

            ], 
            ImagensInternet: [

                "https://tntsports.com.br/__export/1591179371726/sites/esporteinterativo/img/2020/06/03/cristiano-melhor-champions.jpg_554688468.jpg",

                "https://s1.1zoom.me/big0/703/Planets_Trees_Night_576489_1280x800.jp",

                "https://s1.1zoom.me/big0/402/Planets_Clouds_558854_1280x720.jpg"
            ]


        };//Fim return 
    },//Fim data

    computed:{
        randomImage()
        {
            return this.imagensLocais[this.randomIndex];
        },//fim randomImage

        randomImageInternet()
        {
            return this.ImagensInternet[this.randomIndexInternet];
        }

    },//Fim computed 

    methods: {
    getRandomImage()
        {
            this.randomIndex=Math.floor(Math.random()*this.imagensLocais.length);
            this.randomIndexInternet=Math.floor(Math.random()*this.ImagensInternet.length);
            console.log(this.randomIndexInternet);
        }
    },//fim methods 
}).mount("#app"); 