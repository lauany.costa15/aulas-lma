const {createApp} = Vue;
createApp({
data(){
    return{

        testeSpan: false,
        isLampadaLigada: false, 

        }//Fim return
    },//Fim data

    methods:{

        handleTest: function()
        {
            this.testeSpan = !this.testeSpan;
        },//Fim handleTest
        toggleLampada: function()
        {
            this.isLampadaLigada = !this.isLampadaLigada;
        }
    },//Fim methods

}).mount("#app");


