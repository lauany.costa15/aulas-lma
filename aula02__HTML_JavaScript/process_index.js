//Prcessamento dos dados do formulário "index.html"

//Acessando os elementos formulário do html
const formulario = document.getElementById("formulario1");

//adiciona um ouvinte de eventos a um elemento HTML
formulario.addEventListener("submit", function(evento)
{
    evento.preventDefault();/*previne o compartamento padrão de um elemento HTML em resposta a um evento*/
    //constantes para tratar os dados recebidos dos elementos do formulario
    const nome = document.getElementById("nome").value;
    const email = document.getElementById("email").value;

    //Exibe um alerta com os dados coletados 
    //alert(`Nome: ${nome} --- E-mail: ${email}`);

    const updateResultado = document.getElementById("resultado");
    updateResultado.value = `Nome: ${nome} ______ E-mail: ${email}`;

    updateResultado.style.widht = updateResultado.scrollWidth + "px";
});